
const macaddress = require('macaddress');
const publicIp = require('public-ip');

macaddress.one( function (err, mac) {
    console.log("Mac address for me: %s", mac);  
});

 
(async () => {
      console.log(await publicIp.v4());
      })();
