const prog = require('caporal');
prog
  .version('1.0.0')
  // you specify arguments using .argument()
  // 'app' is required, 'env' is optional
  .command('todo', 'Set a todo for pushing to')
    .argument('<what>','What is it you want to do')
  .argument('[type]', 'Type - can be appointment etc', /^appt|impt|task|email$/)
  .option('--context', 'Optional Additional text for contexts')
  .option('--priority <number>', 'Set priority <number> from 1-4', prog.INT)
  .action(function(args, options, logger) {
    if(args.type == 'appt') {
      logger.info('Got appt')
    }
    if(args.what) {
      logger.info(encodeURIComponent(args.what))
    }
  });

prog.parse(process.argv);
